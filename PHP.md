# PHPコーディングルール

<!-- vim:set ft=markdown: -->

## 前提事項
[PSR-0](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md)、[PSR-1](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md)、[PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)に準拠すること。

細則については、フレームワークやモジュールの記法を参照すること。

参考: [php-fig](http://www.php-fig.org/)

### バージョンなど。

* 納品先サーバーの環境に依存する部分もありますが、PHP5.3以上を標準環境とします。なお、シェアコトの受託案件のほとんどを占めるFacebook案件においては、facebook-php-sdkの必要環境は、PHP5.2.0以上、curlモジュール、jsonモジュールが必須です。CentOS6がリリースされてからこの条件が満たせないサーバーはあまり見かけなくなりましたが、注意が必要です。
* フレームワーク、ライブラリ、モジュールなどは、少なくともメジャーバージョンの最新版を使うこと。年単位でメンテナンスされていないようなものは避けること。

## 社内ルール

### 命名ルール
#### クラス名
PSR-0準拠のクラス名ベンダープレフィックスは"Sharecoto"とします。

Composerを使う場合は、vendor/Sharecoto以下にクラスファイルを配置し、`composer.json`に以下のように追記し、`$ composer.phar dump-autoload`すればOKです。それ以外の場合は各ライブラリのオートローダーのルールに任せます。

    "autoload": {
      "psr-0": {
          "Sharecoto": "vendor"
      }
    }

なお、namespaceを使うのか、クラス名をアンダーバーでつなぐ、いわゆる「擬似namespace」を使うのか、の判断は、環境依存の側面もあるので、当面は任意とします。

### 標準ライブラリ
#### セキュリティ
* [phpass](http://www.openwall.com/phpass/) パスワード管理。WordpressやDrupalで使われている。

#### WebAPI
* [Facebook SDK for PHP](https://github.com/facebook/facebook-php-sdk)

### 推奨ライブラリ

#### ウェブアプリケーションフレームワーク
* [Zend Framework 1](http://framework.zend.com/manual/1.12/ja/manual.html)
  ZF2はコードジェネレーターなどの環境が整ってないのでまだ使いづらい。当面は1.12系で。

* [Slim Framework](http://slimframework.com/)
  マイクロフレームワーク。ORMもフォームのバリデータも含まれてないので別途用意しなくてはいけないが、ちょっとした物を手早く作るにはよい

#### セキュリティ
* [HTML Purifier](http://htmlpurifier.org/)
  XSS対策。URLぽい文字列をリンクにするとかいう使い方もできて便利

#### ライブラリ・パッケージ管理
* [Composer](http://getcomposer.org/)

#### 画像処理
* [PHP Image Workshop](http://phpimageworkshop.com/)
  GDしか無いときに便利。画像をレイヤーオブジェクトのように扱える

#### グラフ・チャート
* [pChart2](http://www.pchart.net/)
  そんなに使いやすいわけではないが、事実上これくらいしか選択肢がない

