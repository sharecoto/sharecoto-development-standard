<?php
require_once('../vendor/autoload.php');

use \Michelf\Markdown;

$config = array(
    'templates.path' => '../templates',
    'mdpath' => '../../'
);

$app = new \Slim\Slim(array_merge(
    $config
));

// デフォルトのヘッダ
$res = $app->response();
$res['Content-Type'] = 'text/html;charset=UTF-8';

$app->get('/', function() use($app) {
    $contents = file_get_contents($app->config('mdpath') . 'README.md');
    $html = Markdown::defaultTransform($contents);

    $app->render('index.phtml', array('html'=>$html, 'app'=>$app));
});

$app->get('/contents/:name', function($name) use($app) {
    $fileName = strtoupper($name) . '.md';
    $contents = file_get_contents($app->config('mdpath') . $fileName);
    $html = Markdown::defaultTransform($contents);

    $app->render('index.phtml', array('html'=>$html, 'app'=>$app));
});

$app->run();

