# レスポンシブ対応開発・テストのTIPS

## 開発環境

最近のFirefoxやChromeでは開発者ツールのエミュレーション機能で```meta```の```viewport```もちゃんと解釈します。

実機での確認が不要になるわけじゃありませんが、十分使えるレベルになっています。

### Chromeで確認する

Chrome32くらいから、開発ツールのエミュレーションモードがおおきく変わりました。

開発ツールの設定画面から「Show 'Emulation' view in console drawer.」を有効（チェック）にします。

開発ツール上でESCキーを押すと表示されるコンソールに「Emulation」タブが追加されています。Deviceタブで適当な機種を選択して、「Emulate」をクリックすると、機種に対応した解像度で表示されます。

エミュレーションモードの画面は下のような感じです。
<img src="../img/chrome_emulate.jpg" />

解像度だけでなくUserAgentも書き換えてくれるので、サーバーサイドのテストにも使えます。

なお、バージョンや設定によっては、正しく描画されない場合があります。以下のURLをアドレスバーにコピペ。それぞれ「有効にする」をクリックすれば行けると思います。

* [chrome://flags/#ignore-gpu-blacklist](chrome://flags/#ignore-gpu-blacklist)
* [chrome://flags/#enable-devtools-experiments](chrome://flags/#enable-devtools-experiments)

### Firefoxで確認する

レイアウトの確認だけならFirefoxのレスポンシブデザインモードが使いやすいです。開発者ツールの右上の方にアイコンがあります。設定不要でクリックするだけ。

ただし、UserAgentの変更とかは出来ない模様。

Firefoxのレスポンシブモードは下のような感じです。

<img src="../img/firefox_responsive.jpg">
