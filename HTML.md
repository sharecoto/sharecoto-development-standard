# HTMLコーディングルール

<!-- vim:set ft=markdown: -->

## 準拠する規格

原則としてHTML5に準拠する。

### バリデーター
納品前にはかならず以下のいずれかのバリデータで妥当性の検討を行うこと。

* [About Another HTML-lint5](http://www.htmllint.net/en/html-lint/htmllint.html)__「ふつうです」以上__必須。
* [W3C Markup Validation Service](http://validator.w3.org/)__「Result:Passed」__必須。
* [HTML-Tidy](http://tidy.sourceforge.net/)などの検証ツールも活用すること

※文法的にValidだからといって「よいHTML」文書とは限りません。適切に構造化された読みやすくメンテナンスしやすい文書を目指しましょう。

## テンプレート

[HTML5 Boilerplate](http://html5boilerplate.com/ "H5BP")や、H5BPから派生した[Initializr](http://www.initializr.com/)などのテンプレートを利用する。

## 文字コード

UTF-8とする。

Shift-JISしか使えないフィーチャーフォン対応が必要な場合も、プログラム側でShift-JISに変換して出力するので、UTF-8でコーディングすること（シェアコトではフィーチャーフォン案件はほとんど無いけど）。

## 改行コード

LFに統一する。

## コーディングスタイル

改行やインデントルールで迷った場合には、Chromeの開発者ツールのElementsもしくはFirebugのHTMLの表示に準拠するようにするとよい。

### HTMLタグ名、属性名はすべて小文字で表記する

    ○
    <p>段落</p>

    ☓
    <P>段落</p>

### 終了タグ省略の禁止

仕様上終了タグの省略が許されている要素についても、終了タグを省略してはいけません。

### クラス名は全て小文字で表記し、1〜3語程度の英単語をアンダースコア(' _ ')でつなぐ

    ○
    class_name

    ☓
    className
    class-name

### IDはすべて小文字で表記し、1〜3語程度の英単語をハイフン(' - ')でつなぐ

    ○
    this-is-id

    ☓
    thisIsId
    this_is_id

### フォームのname属性は、1〜3語程度の英単語をハイフンでつなぐ

    ○
    user-name

    ☓
    userName
    user_name

フォームを自動生成するモジュールを使っていたりすると守り切れない場合もありますが、HTMLコーディングの段階では上記のルールでおねがいします。

#### クラス名、IDなどの命名時の注意点
* 特別な理由がない限り、日本語ローマ字表記は禁止します。ローマ字の表記揺れは厄介な問題を引き起こしがちです。
* 英単語はなるべく省略しないこと。意味がわからなくなりがちです。
* 要素の__意味__や__役割__に基づいた名前をつけること。特に、レイアウトや配色に基づいた名前をつけては*いけません*。たとえば`class="red"`などと書かないこと（あとからその部分をまとめて青にしたくなった時にどうしますか？）。
* また、数字と要素の意味が直接関連するような場合を除き、classやidに数字を含めてはいけません。数字順に意味がある場合とは例えば、診断アプリの質問項目などです。
* 上記ルールに反しますが、TwitterBootstrapなどのようなCSSフレームワークを使う場合は、フレームワークのグリッドレイアウト系クラス（`span*`などの）やサイズ指定系のクラス（`btn-mini`とか）はしょうがないので許可します。

### 属性値はダブルクオート（`""`）で括る

どちらでもいいと言えばどちらでもいいのだけど、シングルクオートとの混在は避けるべき。引用符の省略は許可しません。

### 属性値の省略について

`checked`、`required`などの、文法上属性値の省略が許されているBoolean属性については、省略しても構いません。

### インデントルール
* インデント幅はスペース2文字分とする。
* TAB文字を使わない
* `<head></head>`内も適切にインデントすること。

※boilerplateはスペース4文字だけど、HTMLはネストが深くなりがちなので、2文字のほうがいいのではないかと。

### 改行ルール
(HTML5以前の規格で言うところの)いわゆるブロック要素の前後は必ず改行する。

    ○
    <dl>
      <dt>Definition Term</dt>
      <dd>Definition Description</dd>
    </dl>

    ☓
    <dl>
      <dt>Definition Term</dt><dd>Definition Description</dd>
    </dl>

子要素を含む場合、親タグの前後で改行するように。ただし、子要素が短く、改行を入れることでかえって可読性を損ねるような場合には入れないほうがよい場合もある。

    ○
    <ul>
      <li>
        <a href="http://example.com">
          < src="image.jpg" class="thumbnail" width="120" alt="thumbnail">
        </a>
      </li>
    </ul>

    ○
    <p><em>強調</em>された文章</p>

    ☓ このくらい長くなるようなら改行を入れたほうがよい
    <ul>
      <li><a href="http://example.com/"><img src="image.jpg" class="thumbnail" width="120" alt="thumbnail"></a></li>
    </ul>

BODY直下のブロックの間には必ず空行を入れること。

    ○
    <body>
      <section>
        <h1>Heading 1</h1>
        <div>
          …
        </div>
      </section>

      <section>
        略
      </section>

      <footer>
        hogehoge
      </footer>
    </body>

タグの途中で改行しない（以下のような書き方は、特定のブラウザで画像を隙間なく並べたい時のバッドノウハウとしてよく使われていた。けど、禁止）。

    ☓
    <img src="thumb1.jpg"
    ><img src="thumb2.jpg"
    ><img src="thumb2.jpg">

## その他
* 文末にスペースを入れてはいけない。
* 縦マージン調整の目的で`<br />`を使ってはいけない。
* 横マージン調整の目的で全角スペースを使ってはいけない。
* もちろん`&nbsp;`の連打も禁止。
* マージン調整や背景調整の目的で空の`div`要素や`span`要素を作ってはいけない。
* 罫線画像のみの`div`も禁止。どうしてもやりたければ`hr`要素のCSSで調整すべし。

## JavaScript関連

Javascriptは文書の後ろに書く。`body`の閉じタグの直前くらいにまとめておくようにするとよい。一般に、そのほうがパフォーマンス面で有利になる。

### onXXX属性の禁止

イベントリスナーを使うこと。jQueryを使えば簡単に書ける。

    ☓
    <a href="#" id="btn-next" onClick="clickFunction()">次へ</a>

    ○
    <a href="#" id="btn-next">次へ</a>
    <!-- 以下略 -->
    <script>
      $('#btn-next').click(function() {
        /* code */
      });
    </script>

### `script`要素内のJS

なるべくsrcでJSファイルを呼び出すようにする。

デバッグが難しくなるなどの弊害があるので、直接`<script></script>`内にJavascriptを書き込むのは避ける。

ただし、PHPのコードを埋め込みたい場合など、外部JSファイルに切り出すのがむずかしい場合もある。この場合は、JSのみ含まれるテンプレートに切り出すようにする。gjslintなどでの検証も容易になる。

また、パフォーマンスの面で、外部JSを読み込むコストが気になる場合は、HTMLファイル内に記述することを検討してよい。
