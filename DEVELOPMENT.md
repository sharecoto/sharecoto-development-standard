# 開発の進め方

<!-- vim:set ft=markdown: -->

## About
開発者全員が留意スべき注意点と、暗黙のルールを明確化します。以下のルールは固定化されるべきものではなく、必要に応じて更新されていくべきものです。内容についての検討は、redmine上のWikiやフォーラムを通じて行うようにしてください。

## 開発環境

標準のVCSとしてgit、共有リポジトリとして[bitbucket](https://bitbucket.org/)上でプライベートリポジトリを利用しています。

プロジェクト管理としてredmineを利用します。
[シェアコトRedmine](http://redmine.sharecoto.net/)

開発サーバーとして、development02.sharecoto.comが利用できます。

## リポジトリの作成
[bitbucket](https://bitbucket.org/)上のsharecotoグループをオーナとしてリポジトリを作成します。

リポジトリの命名ルールはclient-app-nameのように、クライアント名を先頭に、１〜３語程度の英単語でアプリ名（プロジェクト名）をハイフンでつなぎます。固有名詞以外の日本語ローマ字表記はなるべく避けましょう。

自社内プロジェクトの場合、クライアント名は"sharecoto"とします。

    例）
    client-app-name
    shareocoto-project-name

アクセスレベルはプライベートリポジトリとし、リポジトリタイプはGitとします。「課題追跡」や「Wiki」はredmine上で共有するので、bitbucket上では必要ありません。

## プロジェクトの作成

redmine上でプロジェクトを作成する場合は、以下の命名ルールを標準とします。
一覧にした時にクライアント名でソートされるように、先頭にクライアント名をおき、具体的なプロジェクト（アプリ）名を後置します。社内プロジェクトの場合は、「社内」を前置するようにします。

## bitbucketとredmineの連携

bitbuketのリポジトリの設定画面で、「Hooks」を以下のように設定します。

    Hooks: POST
    URL: http://development.sharecoto.com/redmine/bitbucketgit_hook

また、以下の設定を追加して、開発サーバーで、apacheの公開範囲にリンクを張ることで、pushと同時に開発サーバー上で確認できるようになります。

    Hooks: POST
    URL: http://development.sharecoto.com/bitbucket_post.php

次に、redmineのプロジェクトの設定画面で、リポジトリの設定を行います。「新しいリポジトリ」をクリックし、「リポジトリのパス」にbitbucketのリポジトリのパスをコピペします。

このとき、bitbucket上のSSHのパスをコピペするようにしないとうまく行かないので注意してください。

「作成」を押して、しばらく待つと、開発サーバー上にリポジトリのミラーが作成されますので、再度「作成」を押せば完了です。

### bitbucket_post.phpを指定した場合の開発サーバー上の設定

上記スクリプトにPOSTリクエストが送られると、開発サーバー上に自動でリポジトリがcloneされます。リポジトリが展開されるのは /var/local/sites/bitbucket/ 以下なので、ここから /var/www 内の任意の名前でリンクをはってください。

## リポジトリの構成について
以下のディレクトリ構成をルール化します。

    materials/
    docs/
    app/

* materials : アプリに組み込まれる前のHTMLや画像など
* docs : 開発者に共有されるべき仕様書などのドキュメント類
* app : アプリケーション本体のソースコード

### Tips: Git運用上の注意点
キャッシュ、ログファイル、アプリケーションが自動的に生成する中間ファイルやバックアップファイルなどがgitのリポジトリに含まれないように、.gitignoreファイルの設定を忘れないように。

特にMacの人は、.gitignoreに
    .DS_Store
を必ず追加すること。

Gitは空のディレクトリを無視するので、ディレクトリ作成時にすべてのディレクトリに.gitignoreファイルを作っておくとよい。
