# modien.IEの仮想マシン利用方法

[modern.IE](http://loc.modern.ie/ja)は、複数バージョンでのIEのテストを容易にするためにMicrosoftが提供するサービスです。

modern.IEでは、VMWarePlayerなどで利用できる仮想マシンのイメージを配布しています。日本語パックが使えないような制限はあるものの、ほぼ実機同様のテストができるようになりました。

IE11の開発者ツールでは再現しないようなクライアントサイドの不具合もありますので、要件に合わせて仮想マシンのインストールを強く推奨します。

ちなみに、modern.IEはIE7で見ようとすると表示が崩れます。Microsoftは本気だ（笑）

## VMWarePlayerのインストール

modern.IEで配布している仮想マシンのイメージはHyper-V、Virtual-PC、VirtualBox、VmWarePlayerにそれぞれ対応しています。

ここではWindows用のVMWarePlayerのインストールについて説明します。

[ダウンロード VMware Player](https://my.vmware.com/jp/web/vmware/free#desktop_end_user_computing/vmware_player/6_0)

上記ページから、「VMware Player and VMware Player Plus for Windows」をダウンロード。

ダウンロードが完了したらVMware-player-__version__.exeというファイルをダブルクリックして画面の指示に従います。

## 仮想マシンのダウンロード

[modern.IE](http://loc.modern.ie/ja/)のページから[無料のツールを入手する](http://loc.modern.ie/virtualization-tools)リンクをたどると、「Mac、Linux、Windows 用の仮想マシンをダウンロードします。」というセクションがあります。

「無料のVMを入手する」をクリックするとホストOSの選択、仮想マシンのプラットフォームを選択するドロップダウンメニューが出てきますので、「目的のOS」を「Windows」「仮想環境のプラットフォーム」を「VMWare Player」にしてください。

2014年2月時点で以下の仮想マシンがVMWare用に配布されています。

* IE6 - XP
* IE8 - XP
* IE7 - Vista
* IE8 - Win7
* IE9 - Win7
* IE10 - Win7
* IE11 - Win7
* IE10 - Win8
* IE11 - Win8.1

必要な環境のpart1.exeからpartX.rarまですべてダウンロードする必要があります。たとえば、IE8-Win7の環境を構築するには、以下のファイルをすべてダウンロードします。

* https://az412801.vo.msecnd.net/vhd/VMBuild_20131127/VMware/IE8_Win7/Windows/IE8.Win7.For.WindowsVMware.part001.exe
* https://az412801.vo.msecnd.net/vhd/VMBuild_20131127/VMware/IE8_Win7/Windows/IE8.Win7.For.WindowsVMware.part002.rar
* https://az412801.vo.msecnd.net/vhd/VMBuild_20131127/VMware/IE8_Win7/Windows/IE8.Win7.For.WindowsVMware.part003.rar
* https://az412801.vo.msecnd.net/vhd/VMBuild_20131127/VMware/IE8_Win7/Windows/IE8.Win7.For.WindowsVMware.part004.rar
* https://az412801.vo.msecnd.net/vhd/VMBuild_20131127/VMware/IE8_Win7/Windows/IE8.Win7.For.WindowsVMware.part005.rar

すべてのファイルのダウンロードが終わったら、part1.exeをダブルクリック。WinRarの自己解凍ダイアログが起動するので、解凍する場所を選択して「extract」をクリックします。覚えやすい場所ならどこでも構いません。

<img src="../img/winrar.jpg" alt="">

すべて展開し終わると、指定したディレクトリに以下のようなファイルができていると思います。

* 環境名.ovf
* 環境名.vmdk

## VMWarePlayerで仮想マシンを実行する

VMWarePlayerを実行すると、以下のような感じの画面が起動します（藤井の環境の画面です。インストールした直後には左側のカラムの「ホーム」以下は空になっているはずなので気にしないでください）。

<img src="../img/vmware.jpg" alt="">

右側の「仮想マシンを開く」をクリックして、先ほど展開した.ovfファイルを指定すると、「仮想マシンのインポート」のダイアログが表示されます。ひとまずデフォルトのままで構わないので、「インポート」をクリックします。インポートが完了すると、左側のカラムに仮想マシンのリストが追加されているはずです。

起動したい仮想マシンを選んでダブルクリックすると、仮想マシンが起動します。

なお、XPでは、日本語フォントを追加しないと表示できません。Win7は表示に関しては問題ありません。ちょっといじってみた感じではWindowsアップデートからの言語パック導入も出来ないみたいなので、ホストマシンのWindowsからMSゴシックを探して仮想マシンのウィンドウにドラッグアンドドロップし、仮想マシン上でダブルクリックするとインストールできるはずです。

