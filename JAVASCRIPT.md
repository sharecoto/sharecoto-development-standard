# JavaScriptコーディングルール
<!-- vim:set ft=markdown: -->

[Google JavaScript Style Guide](http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml)を採用する。

文法チェック、コーディングスタイルのチェックには、[Closure Linter](https://developers.google.com/closure/utilities/docs/linter_howto?hl=ja)を使う。
