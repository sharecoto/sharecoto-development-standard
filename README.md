# シェアコト開発標準（草案）

## About

シェアコトにおける開発のすすめかたや、コーディングルールについて記述したものです。また、必要に応じて関連するTipsも紹介します。

特に理由のない限り、各プロジェクトはルールにしたがって開発を進めてください（特別な理由とは、レビュワーに論理的に説明でき、かつ説得力がある、という意味です）。

以下にあるルールは固定化されるべきものではなく、常に必要に応じて更新されていくべきものです。しかしながら、議論なくルール変更してはいけません。内容に関する検討は、redmine上のWikiやフォーラムで進めていく事とします。

## Contents

* [README.md](./) : この文書
* [DEVELOPMENT.md](contents/development) : 開発の進め方について
* [NAMING.md](contents/naming) : 各種命名ルールまとめ
* [HTML.md](contents/html) : HTMLマークアップについて
* [JAVASCRIPT.md](contents/javascript) : JavaScript開発ルール
* [PHP.md](contents/php) : PHP開発ルール

### Tips
* [ModernIe.md](contents/modernie)
* [Responsive.md](contents/responsive)

## 参考リンク
この草案を作成するにあたって、既存のコーディング規約の多くを参考にしています。ここに挙げるのはその代表的なものです。

* [GitHubスタイルガイド](https://github.com/styleguide)
* [PHP-fig](http://www.php-fig.org/)
* [Google JavaScript Style Guide](http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml)
